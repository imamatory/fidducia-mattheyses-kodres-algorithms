from igraph import Graph, plot
from collections import defaultdict
from copy import deepcopy


def read_from_gml(file_name):
    f = open(file_name, encoding="utf-8")
    graph = Graph.Read_GML(f)
    f.close()
    return graph


def get_ids_by_indexes(g, ids):
    return [int(v) for v in g.vs.select(ids)["id"]]


def max_con(from_vts, to_vts, excluded_ids=None):
    if from_vts.graph == to_vts.graph:
        graph = from_vts.graph
        if excluded_ids is None:
            excluded_ids = []
        out_edges = set()
        s1 = [v.index for v in from_vts]
        s2 = [v.index for v in to_vts]
        for v1 in s1:
            for v2 in graph.neighbors(v1):
                if v2 in s2:
                    out_edges.add(graph.get_eid(v1, v2))
        g_degs = graph.subgraph_edges(out_edges, delete_vertices=False)
        # plot(g_degs)
        if max(g_degs.degree()) > 0:
            degrees = {v.index: v.degree()
                       for v in g_degs.vs.select(id_notin=[v for v in g_degs.vs.select(excluded_ids)["id"]],
                                                 _degree_gt=0)}
            if len(degrees) != 0:
                max_degree = max(iter(degrees.items()), key=lambda x: x[1])[1]
                return [x[0] for x in degrees.items() if x[1] == max_degree]
            else:
                return [-1]
        else:
            not_related = [v.index for v in to_vts if v.index not in excluded_ids]
            return not_related if len(not_related) > 0 else [-1]
    else:
        raise Exception("Parameters must related with a single graph.")


def min_con(vts_ids):
    full_degree = {i: d for i, d in zip(vts_ids, A.degree(vts_ids))}
    return min(iter(full_degree.items()), key=lambda x: x[1])[:1]


def calc_area(vts, hyper_e):
    area = 0
    for v in vts:
        hyper_e_set = set(hyper_e.keys())
        for i in vts.graph.es.select(vts.graph.incident(v))["hyper"]:
            if i == 0 or i in hyper_e_set:
                hyper_e_set.discard(i)
                area += 1
    return area


def calc_outputs(vts, hyper_e):
    repeated_edges = vts.graph.es.select(_within=[v.index for v in vts])
    common_outs_num = 0
    for v in repeated_edges["hyper"]:
        if v in hyper_e.keys():
            common_outs_num += 1
    return calc_area(vts, hyper_e) - 2 * len(repeated_edges) + common_outs_num


g = read_from_gml("seminar.txt")

# mark hidden vertices as peripheral and make it blue
colors = ('red', 'blue')
g.vs["peripheral"] = [i for i in g.vs["hidden"]]
g.vs["color"] = [colors[int(i)] for i in g.vs["peripheral"]]

# plot(g)

# find and group hyper-edges
hyper_edges = defaultdict(list)
for e_group in set(g.es["hyper"]):
    if e_group != 0:
        for edge in g.es:
            if edge["hyper"] == e_group:
                hyper_edges[int(e_group)].append(edge.index)


# start the algorithm
S = 8
T = 6

A = deepcopy(g)
blocks = []
block_buff = []
excluded_vts = []
# plot(g)
while len(A.vs.select(peripheral=0)) > 0:
    a = max_con(A.vs.select(peripheral=0), A.vs.select(peripheral=1))
    a = [v.index for v in A.vs.select(a) if v["peripheral"] == 0]
    if len(a) > 1:
        a = min_con(a)
    block_buff.append(a[0])
    excluded_vts.append(a[0])
    while 1:
        f = get_ids_by_indexes(A, block_buff)
        a = max_con(A.vs.select(id_in=f),
                    A.vs.select(id_notin=f, peripheral=0),
                    excluded_ids=excluded_vts)[0]
        if a == -1:
            break

        block_buff.append(a)
        excluded_vts.append(block_buff[-1])
        if calc_outputs(A.vs.select(block_buff), hyper_edges) > T\
                or calc_area(A.vs.select(block_buff), hyper_edges) > S:
            block_buff.pop()
    blocks.append(block_buff.copy())
    block_buff.clear()
    excluded_vts.clear()
    row_block = [j for i in blocks for j in i]
    A.vs.select(row_block)["peripheral"] = 1
    A.vs["color"] = [colors[int(i)] for i in g.vs["peripheral"]]
    # plot(A)

block_ids = []
buff = []
for b in blocks:
    for id in g.vs.select(b)["id"]:
        buff.append(int(id))
    block_ids.append(buff.copy())
    buff.clear()

print(block_ids)

# plot(g.intersection(g.induced_subgraph(A.vs.select(id_in=blocks))))
plot(g)