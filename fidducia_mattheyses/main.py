from igraph import Graph, plot, Vertex
from operator import itemgetter


def read_from_gml(file_name):
    f = open(file_name, encoding="utf-8")
    g = Graph.Read_GML(f)
    f.close()
    return g


class FidduciaMattheyses:

    def __init__(self, file_name, r):
        self.graph = read_from_gml(file_name)
        self.r = r
        self.hyper_edges = self.graph.es["hyper"]
        self.max_area = max([self.__count_area(v) for v in self.graph.vs])
        self.area_mid = r * self.__count_area(self.graph.vs)
        self.groups = ("A", "B")

    def plot(self):
        # mark A as red and B as blue
        colors = {"A": 'red', "B": 'blue'}
        self.graph.vs["color"] = [colors[i] for i in self.graph.vs["group"]]
        plot(self.graph)

    def __count_area(self, vts):
        area = 0
        # научить работать с неитерируемыми объектами
        if type(vts) is Vertex:
            vts = (vts,)
        for v in vts:
            hyper_e_set = set(self.hyper_edges)
            for i in self.graph.es.select(self.graph.incident(v))["hyper"]:
                if i == 0 or i in hyper_e_set:
                    hyper_e_set.discard(i)
                    area += 1
        return area

    def __count_fs(self, v):
        fs = 0
        hyper_white, hyper_black = set(), set()
        for i in self.graph.vs(self.graph.neighbors(v)):
            h = self.graph.es.find(_between=((i.index,), (v.index,)))["hyper"]
            if i["group"] != v["group"]:
                if h == 0:
                    fs += 1
                else:
                    if h not in hyper_black:
                        hyper_white.add(h)
            else:
                if h != 0:
                    hyper_black.add(h)

        fs += len(hyper_white.difference(hyper_black))
        return fs

    def __count_te(self, v):
        te = 0
        hyper_white, hyper_black = set(), set()
        for i in self.graph.vs(self.graph.neighbors(v)):
            h = self.graph.es.find(_between=((i.index,), (v.index,)))["hyper"]
            if i["group"] == v["group"]:
                if h == 0:
                    te += 1
                else:
                    if h not in hyper_black:
                        hyper_white.add(h)
            else:
                if h != 0:
                    hyper_black.add(h)

        te += len(hyper_white.difference(hyper_black))
        return te

    def get_state(self):
        return [x for x in zip(self.graph.vs["id"], self.graph.vs["group"])]

    def get_partition(self):
        included_vts = [v for v in self.graph.vs]
        min_a, max_a = self.area_mid - self.max_area, self.area_mid + self.max_area
        while True:
            # self.plot()
            deltas = [self.__count_fs(v) - self.__count_te(v) for v in included_vts]
            if deltas and max(deltas) < 1:
                return self.get_state()

            vts_deltas = [(v, d) for (v, d) in zip(included_vts, deltas) if d > 0]
            vts_deltas = sorted(vts_deltas, reverse=True, key=itemgetter(1))

            area_after_replacing = {}
            was_replaced = False

            for cur_delta in sorted(set([x[1] for x in vts_deltas])):
                for vd in [x for x in vts_deltas if x[1] == cur_delta]:
                    a = self.__count_area(self.graph.vs.select(group=self.groups[0]))

                    if vd[0]["group"] == self.groups[0]:
                        a -= self.__count_area(vd[0])
                    else:
                        a += self.__count_area(vd[0])

                    area_after_replacing[vd[0]] = a

                for x in area_after_replacing.copy().items():
                    if not (min_a <= x[1] <= max_a):
                        del area_after_replacing[x[0]]

                if area_after_replacing:
                    replacing_v = min(area_after_replacing.items(), key=lambda i: abs(i[1] - self.area_mid))[0]
                    if self.groups[0] == replacing_v["group"]:
                        replacing_v["group"] = self.groups[1]
                    else:
                        replacing_v["group"] = self.groups[0]
                    included_vts.pop(included_vts.index(replacing_v))
                    was_replaced = True
                else:
                    if cur_delta == 1:
                        return self.get_state()
                    area_after_replacing.clear()

                if was_replaced:
                    break


obj = FidduciaMattheyses("var11.txt", 0.5)
obj.plot()
print(obj.get_partition())
obj.plot()

