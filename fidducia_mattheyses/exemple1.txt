graph [
	comment "This is a sample graph"
	directed 0
	label "Hello, I am a graph"
	node [
		id 1
		label "node 1"
		group "A"
	]
	node [
		id 2
		label "node 2"
		group "A"
	]
	node [
		id 3
		label "node 3"
		group "A"
	]
	node [
		id 4
		label "node 4"
		group "B"
	]
	node [
		id 5
		label "node 5"
		group "B"
	]
	node [
		id 6
		label "node 6"
		group "B"
	]
	edge [
		source 1
		target 2
		hyper 1
		label "b"
	]
	edge [
		source 1
		target 4
		hyper 1
		label "b"
	]
	edge [
		source 2
		target 4
        hyper 1
		label "b"
	]
	edge [
		source 2
		target 3
		hyper 2
		label "c"
	]
	edge [
		source 2
		target 5
		hyper 2
		label "c"
	]
	edge [
		source 2
		target 6
		hyper 2
		label "c"
	]
	edge [
		source 3
		target 5
		hyper 2
		label "c"
	]
	edge [
		source 5
		target 6
		hyper 2
		label "c"
	]
	edge [
		source 3
		target 6
		label "a"
	]
	edge [
		source 3
		target 6
		hyper 2
		label "c"
	]
]